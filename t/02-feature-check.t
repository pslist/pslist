#!/usr/bin/perl
#
# Copyright (c) 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.010;
use strict;
use warnings;

use Test::More;
use Test::Command;

use lib 't/lib';
use Test::PSList qw(
	env_init
	get_error_output get_ok_output
);

my %c = env_init;

# First, let's see if feature-check is even there...
{
	my $cmd = Test::Command->new(
	    cmd => ['feature-check', 'feature-check', 'feature-check']);
	my $value = $cmd->exit_value;
	if (!defined $value) {
		plan skip_all => 'The feature-check tool is not installed';
	} elsif ($value != 0) {
		BAIL_OUT("The feature-check tool's self-check failed with ".
		    "code $value");
	}
}

plan tests => 3;

subtest 'Check for the pslist feature' => sub {
	plan tests => 2;
	get_ok_output(['feature-check', '--', $c{prog}, 'pslist'],
	    'check for the "pslist" feature');
};

subtest 'pslist feature version >= 1' => sub {
	plan tests => 2;
	get_ok_output(['feature-check', '--', $c{prog}, 'pslist', 'ge', '1'],
	    'the "pslist" feature >= 1');
};

subtest 'pslist feature version < 1000' => sub {
	plan tests => 2;
	get_ok_output(['feature-check', '--', $c{prog}, 'pslist lt 1000'],
	    'the "pslist" feature < 1000');
};
