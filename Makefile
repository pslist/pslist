# Copyright (c) 2000, 2005, 2009, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# install recipe
INSTALL		?= install
INSTALL_COPY	?= -c
INSTALL_OWN	?= -o root
INSTALL_MODE_S	?= -m 555
INSTALL_MODE_D	?= -m 644
INSTALL_SCRIPT	?= ${INSTALL} ${INSTALL_COPY} ${INSTALL_OWN} ${INSTALL_MODE_S}
INSTALL_DATA	?= ${INSTALL} ${INSTALL_COPY} ${INSTALL_OWN} ${INSTALL_MODE_D}

# where to install
PREFIX		?= /usr/local
BINDIR		?= ${PREFIX}/bin
MANDIR		?= ${PREFIX}/man/man
MAN1DIR		?= ${MANDIR}1

# some other useful programs
ECHO		?= echo
FALSE		?= false
GZIP		?= gzip -c9
LN		?= ln
MKDIR		?= mkdir -p
RM		?= rm -f

PROG		= pslist
RKILL		= rkill
RRENICE		= rrenice
MAN1		= ${PROG}.1
MAN1GZ		= ${PROG}.1.gz

all:	${MAN1GZ}

install:
	${MKDIR} ${DESTDIR}${BINDIR}
	${INSTALL_SCRIPT} ${PROG} ${DESTDIR}${BINDIR}
	${LN} -s ${PROG} ${DESTDIR}${BINDIR}/${RKILL}
	${LN} -s ${PROG} ${DESTDIR}${BINDIR}/${RRENICE}
	${MKDIR} ${DESTDIR}${MAN1DIR}
	${INSTALL_DATA} ${MAN1GZ} ${DESTDIR}${MAN1DIR}
	${LN} -s ${MAN1GZ} ${DESTDIR}${MAN1DIR}/${RKILL}.1.gz
	${LN} -s ${MAN1GZ} ${DESTDIR}${MAN1DIR}/${RRENICE}.1.gz

${MAN1GZ}:	${MAN1}
	${GZIP} ${GZIP_ARGS} ${MAN1} > ${MAN1GZ} || (${RM} ${MAN1GZ}; ${FALSE})

clean:
	${RM} ${MAN1GZ}

test:	all
	prove t
